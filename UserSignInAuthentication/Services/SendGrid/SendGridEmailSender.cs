﻿using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UserSignInAuthentication.Bootstrap;
using UserSignInAuthentication.Core.IService;
using UserSignInAuthentication.Core.Model;

namespace UserSignInAuthentication.Services.SendGrid
{
    public class SendGridEmailSender : ISendGridEmailSender
    {
        public  async Task<SendEmailResponse> SendEmailAsync(SendEmailDetails senderDetails)
        {
            var client = new SendGridClient(IoCContainer.Configuration["SendGridKey"]);

            var from = new EmailAddress { Email = senderDetails.FromEmail, Name = senderDetails.FromName };
            var to = new EmailAddress { Email = senderDetails.ToEmail, Name = senderDetails.ToName };

            var emailHelper = MailHelper.CreateSingleEmail(
                from,
                to, 
                senderDetails.Subject,
                senderDetails.IsHtml ? null: senderDetails.Contents,
                senderDetails.IsHtml ? senderDetails.Contents : null
                );

            var response = await client.SendEmailAsync(emailHelper);

            if(response.StatusCode == HttpStatusCode.Accepted)
            {
                return new SendEmailResponse();
            }

            try
            {
                var body = await response.Body.ReadAsStringAsync();

                var sendGridResponse = JsonConvert.DeserializeObject<SendGridResponse>(body);

                var errorResponse = new SendEmailResponse
                {
                    Errors = sendGridResponse.Errors?.Select(x => x.Message).ToList()
                };

                return errorResponse;

            }
            catch(Exception ex)
            {
                if (Debugger.IsAttached)
                {
                    var exceptionMessage = ex;
                    Debugger.Break();
                }

                return new SendEmailResponse
                {
                    Errors = new List<string> { "Unknown error occurred" }
                };
            }
        }
    }
}
