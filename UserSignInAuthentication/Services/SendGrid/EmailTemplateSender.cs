﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UserSignInAuthentication.Core.IService;
using UserSignInAuthentication.Core.Model;

namespace UserSignInAuthentication.Services.SendGrid
{
    public class EmailTemplateSender : IEmailTemplateSender
    {
        private readonly ISendGridEmailSender _sendGridEmailSender;

        public EmailTemplateSender(ISendGridEmailSender sendGridEmailSender)
        {
            _sendGridEmailSender = sendGridEmailSender;
        }

        public async Task<SendEmailResponse> SendGeneralEmailAsync(SendEmailDetails sendEmail, string title, string content1, string content2, string buttonText, string buttonUrl)
        {
            var template = default(string);

            // Flat data access
            using (var reader = new StreamReader(Assembly.GetEntryAssembly().GetManifestResourceStream("UserSignInAuthentication.Web.Services.SendGrid.GeneralTemplate.htm"), Encoding.UTF8))
            {

                template = await reader.ReadToEndAsync();
            };

            template.Replace("title", title)
                .Replace("content1", content1)
                .Replace("content2", content2)
                .Replace("buttonText", buttonText)
                .Replace("buttonUrl", buttonUrl);
            sendEmail.Contents = template;

            return await _sendGridEmailSender.SendEmailAsync(sendEmail);
        }
    }
}
