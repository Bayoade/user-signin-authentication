﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserSignInAuthentication.Core.IService;

namespace UserSignInAuthentication.Services
{
    public class UserContext : IUserContext
    {
        private readonly HttpContext _context;

        public UserContext(HttpContext context)
        {
            _context = context;
        }

        public bool IsAuthenticated()
        {
            return _context.User.Identity.IsAuthenticated;
        }

        public string UserName()
        {
            return _context.User.Identity.Name;
        }
    }
}
