﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using UserSignInAuthentication.Data.Sql.Context;
using UserSignInAuthentication.Data.Sql.Model;

namespace UserSignInAuthentication.Bootstrap
{
    public static class ConfigureIdentityAuthentication
    {
        public static void AddIdentityUserAuthentication(this IServiceCollection services)
        {
            // Identity adds cookies based Authentication
            // Adds scoped classses for things like UserManager, SigInManager, Password
            // Notes: Automatically adds the validated user from a cookie to the HttpContext.User
            services.AddIdentity<ApplicationUser, IdentityRole>()

                // Add UserStore and RoleStore from this context
                // That are consumed by the userStore and the RoleStore
                .AddEntityFrameworkStores<ApplicationDbContext>()

                // Adds a provider that generates unique keys and hashes for things like
                // forgot password links, phone numbers verification codes etc ..
                .AddDefaultTokenProviders();

            // Configuration for Password requirements
            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            // Alter application cookie
            services.ConfigureApplicationCookie(options =>
            {
                // Rdirect to login
                options.LoginPath = "/login";

                // change cookie time out to 10 seconds
                options.ExpireTimeSpan = TimeSpan.FromMinutes(10);
            });
        }
    }
}
