﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserSignInAuthentication.Core.IService;
using UserSignInAuthentication.Data.Sql.Context;

namespace UserSignInAuthentication.Bootstrap
{
    public static class IoC
    {
        public static ApplicationDbContext ApplicationDbContext => IoCContainer
            .Provider.
            GetService<ApplicationDbContext>();

        public static IEmailTemplateSender EmailTemplateSender => IoCContainer
            .Provider
            .GetService<IEmailTemplateSender>();
    }

    public static class IoCContainer
    {
        public static IServiceProvider Provider { get; set; }

        public static IConfiguration Configuration { get; set; }
    }
}
