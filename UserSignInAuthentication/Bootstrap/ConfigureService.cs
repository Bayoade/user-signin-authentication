﻿using Microsoft.Extensions.DependencyInjection;
using UserSignInAuthentication.Core.IService;
using UserSignInAuthentication.Services;
using UserSignInAuthentication.Services.SendGrid;

namespace UserSignInAuthentication.Bootstrap
{
    public static class ConfigureService
    {
        public static void AddServicesConfigurations(this IServiceCollection services)
        {
            services.AddTransient<IEmailTemplateSender, EmailTemplateSender>();
            services.AddScoped<IUserContext, UserContext>();
            services.AddScoped<ISendGridEmailSender, SendGridEmailSender>();
        }
    }
}
