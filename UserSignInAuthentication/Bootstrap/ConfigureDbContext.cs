﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using UserSignInAuthentication.Data.Sql.Context;

namespace UserSignInAuthentication.Bootstrap
{
    public static class ConfigureDbContext
    {
        public static void AddDbContextCongfigurations(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
            {
                options.UseSqlServer(connectionString);
            });
            
        }
    }
}
