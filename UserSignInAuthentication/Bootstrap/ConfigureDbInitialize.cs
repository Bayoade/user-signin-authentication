﻿using Microsoft.AspNetCore.Builder;

namespace UserSignInAuthentication.Bootstrap
{
    public static class ConfigureDbInitialize
    {
        public static void AddDefaultTables(this IApplicationBuilder app)
        {
            var dbContext = IoC.ApplicationDbContext;
            dbContext.Database.EnsureCreated();
        }
    }
}
