﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserSignInAuthentication.Web.Helpers
{
    public static class IdentityErrorExtensions
    {
        public static string AggregateErrors(this IEnumerable<IdentityError> identityErrors)
        {
            return identityErrors?.ToList().Select(x => x.Description).Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");
        }
    }
}
