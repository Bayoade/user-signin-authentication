﻿namespace UserSignInAuthentication.Web.Helpers
{
    public static class ApiRoutes
    {
        public const string GetUserProfile = "api/user/profile";

        public const string CreateUserAsync = "api/register";

        public const string UpdateUserProfileAsync = "api/user/update";

        public const string UpdateUserProfilePasswordAsync = "api/user/updatepassword";

        public const string VerifyEmail = "verify/email/{userId}/{emailToken}";

        public const string Login = "api/login";
    }
}
