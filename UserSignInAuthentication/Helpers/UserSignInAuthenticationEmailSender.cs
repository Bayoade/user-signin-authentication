﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserSignInAuthentication.Bootstrap;
using UserSignInAuthentication.Core.IService;
using UserSignInAuthentication.Core.Model;

namespace UserSignInAuthentication.Web.Helpers
{
    public static class UserSignInAuthenticationEmailSender
    {

        public static Task<SendEmailResponse> SendVerificationEmail(string displayedEmail, string displayedName, string verificationUrl)
        {

            // Sending a verification email to the specified user;

            return IoC.EmailTemplateSender.SendGeneralEmailAsync(new SendEmailDetails
            {
                Contents = "This is our first Email",
                FromEmail = IoCContainer.Configuration["UserSignInSettings: SenderEmail"],
                FromName = IoCContainer.Configuration["UserSignInSettings: SenderName"],
                IsHtml = true,
                Subject = "Verify your Email - User Sign In",
                ToEmail = displayedEmail,
                ToName = displayedName 
            },
            "Verify Email",
            $"Hi {displayedName ?? "Anonymous User"}",
            "Thanks for creating an account with us<br> To continue click below",
            "Verify Email",
            verificationUrl);
        }

    }
}
