﻿
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using UserSignInAuthentication.Bootstrap;

namespace UserSignInAuthentication.Helpers
{
    public static class TokenGenerator
    {
        public static JwtSecurityToken GenerateJwtToken(string username, string email, string userId)
        {
            // set claims
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.Email, email),
                new Claim(ClaimsIdentity.DefaultNameClaimType, username),

                // the usermanager.getuserasync will get user based on the nameidentitifer
                new Claim(ClaimTypes.NameIdentifier, userId)
            };

            // set credentials
            var credentials = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(IoCContainer.Configuration["FrameworkDI.JWT:SecretKey"])),
                SecurityAlgorithms.HmacSha256);

            // set tokens
            return new JwtSecurityToken(
                issuer: IoCContainer.Configuration["JWT:JwtIssuer"],
                audience: IoCContainer.Configuration["JWT:JwtAudience"],
                claims: claims,
                expires: DateTime.Now.AddMonths(3),
                signingCredentials: credentials
                );
        }
    }
}
