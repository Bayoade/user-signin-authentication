﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace UserSignInAuthentication.Helpers
{
    public class AuthorizeTokenAttributeAttribute : AuthorizeAttribute
    {
        public AuthorizeTokenAttributeAttribute()
        {
            //IdentityConstants.ApplicationScheme
            AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme;
        }
    }
}
