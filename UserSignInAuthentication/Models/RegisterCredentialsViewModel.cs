﻿namespace UserSignInAuthentication.Web.Models
{
    public class RegisterCredentialsViewModel
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
