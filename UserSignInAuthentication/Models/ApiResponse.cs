﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserSignInAuthentication.Core.Helpers;

namespace UserSignInAuthentication.Web.Models
{
    public class ApiResponse
    {
        public ApiResponse()
        {

        }

        public bool Successful => ErrorMessage.IsNull();

        public string ErrorMessage { get; set; }

        public object Response { get; set; }

        
    }

    public class ApiResponse<T> : ApiResponse
    {
        public new T Response { get => (T)base.Response; set => base.Response = value; }

    }
}
