﻿namespace UserSignInAuthentication.Web.Models
{
    public class UpdateUserProfilePasswordApiViewModel
    {
        public string CurrentPassword { get; set; }

        public string  NewPassword { get; set; }
    }
}
