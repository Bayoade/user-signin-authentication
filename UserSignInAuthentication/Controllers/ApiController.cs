﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using UserSignInAuthentication.Core.Helpers;
using UserSignInAuthentication.Data.Sql.Model;
using UserSignInAuthentication.Helpers;
using UserSignInAuthentication.Web.Helpers;
using UserSignInAuthentication.Web.Models;

namespace UserSignInAuthentication.Web.Controllers
{
    /// <summary>
    /// Manages the API Controller
    /// </summary>
    /// 
    [ApiController]
    [AuthorizeTokenAttribute]
    public class ApiController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public ApiController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        [Route(ApiRoutes.Login)]
        [AllowAnonymous]

        public async Task<ApiResponse<RegistrationResultApiModel>> Login([FromBody] LoginViewModel model)
        {
            var apiResponse = new ApiResponse<RegistrationResultApiModel>();
            var userFirstName = default(string);
            var userLastName = default(string);
            var userEmail = default(string);
            var token = new JwtSecurityToken();
            var response = await _signInManager
                .PasswordSignInAsync(model.Password, model.UserName, true, true);

            if (response.Succeeded)
            {
                var userIdentity = await _userManager
                    .FindByNameAsync(model.UserName);
                token = TokenGenerator.GenerateJwtToken(model.UserName, userIdentity.Email, userIdentity.Id);
                userFirstName = userIdentity.FirstName;
                userLastName = userIdentity.LastName;
                userEmail = userIdentity.Email;
            }

            apiResponse.Response = new RegistrationResultApiModel
            {
                FirstName = userFirstName,
                LastName = userLastName,
                Email = userEmail,
                Token = new JwtSecurityTokenHandler().WriteToken(token)
            };

            return apiResponse;
        }

        [Route(ApiRoutes.VerifyEmail)]
        [AllowAnonymous]
        public async Task<IActionResult> VerifyEmail(string userId, string emailToken)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user.IsNull())
            {
                //TODO: Nice UI
                return Content("User not found", "text/html");
            }

            emailToken = emailToken.Replace("%2f", "/").Replace("%2F", "/");

            var result = await _userManager.ConfirmEmailAsync(user, emailToken);

            if (result.Succeeded)
            {
                // TODO: Nice UI
                return Content("Email Verified");
            }

            // TODO: Nice UI
            return Content("Invalid Email Token");
        }

        [AllowAnonymous]
        [Route(ApiRoutes.CreateUserAsync)]
        public async Task<ApiResponse<RegistrationResultApiModel>> CreateUserAsync([FromBody] RegisterCredentialsViewModel registerCredentials)
        {
            var applicationUser = new ApplicationUser
            {
                UserName = registerCredentials.UserName,
                FirstName = registerCredentials.FirstName,
                LastName = registerCredentials.LastName,
                Email = registerCredentials.Email
            };

            var result = await _userManager.CreateAsync(applicationUser, registerCredentials.Password);

            if (result.Succeeded)
            {
                var userIdentity = await _userManager.FindByNameAsync(registerCredentials.UserName);

                await SendEmailVerificationAsync(userIdentity);

                JwtSecurityToken token = TokenGenerator
                    .GenerateJwtToken(userIdentity.UserName, userIdentity.Email, userIdentity.Id.ToString());

                return new ApiResponse<RegistrationResultApiModel>
                {
                    Response = new RegistrationResultApiModel
                    {
                        FirstName = userIdentity.FirstName,
                        LastName = userIdentity.LastName,
                        Email = userIdentity.Email,
                        Token = new JwtSecurityTokenHandler().WriteToken(token)
                    }
                };
            }

            return new ApiResponse<RegistrationResultApiModel>
            {
                ErrorMessage = result.Errors.AggregateErrors()
            };
        }

        [Route(ApiRoutes.GetUserProfile)]
        public async Task<ApiResponse<UserProfileResultApiModel>> GetUserProfile()
        {
            var errors = new List<string>();
            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user.IsNull())
            {
                return new ApiResponse<UserProfileResultApiModel>
                {
                    ErrorMessage = "User Not Found"
                };
            }

            return new ApiResponse<UserProfileResultApiModel>
            {
                Response = new UserProfileResultApiModel
                {
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    UserName = user.UserName

                }
            };
        }

        [Route(ApiRoutes.UpdateUserProfileAsync)]
        public async Task<ApiResponse> UpdateUserProfileAsync([FromBody] UpdateUserProfileViewModel updateUser)
        {
            var errors = new List<string>();
            var emailChanged = false;

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user.IsNull())
            {
                return new ApiResponse
                {
                    ErrorMessage = "User Not Found"
                };
            }

            user.FirstName = updateUser.FirstName;
            user.LastName = updateUser.LastName;

            if(user.Email != updateUser.Email)
            {
                user.Email = updateUser.Email;
                emailChanged = true;
            }
            
            user.UserName = updateUser.UserName;

            var result = await _userManager.UpdateAsync(user);

            if (result.Succeeded && emailChanged)
            {
                await SendEmailVerificationAsync(user);
            }

            if (result.Succeeded)
            {
                return new ApiResponse();
            }

            return new ApiResponse
            {
                ErrorMessage = result.Errors.AggregateErrors()
            };
        }

        private async Task SendEmailVerificationAsync(ApplicationUser userIdentity)
        {
            var emailVerification = _userManager.GenerateEmailConfirmationTokenAsync(userIdentity);
            var emailVerificationCode = await _userManager
                    .GenerateEmailConfirmationTokenAsync(userIdentity);

            //TODO: Replace with APIPoutes that wull contain the static routes to us
            var confirmationUrl = $"http://{Request.Host.Value}/api/verify/email/{HttpUtility.UrlEncode(userIdentity.Id)}/{HttpUtility.UrlEncode(emailVerificationCode)}";

            // TODO: Email code to be send to the new sign up user
            await UserSignInAuthenticationEmailSender
                .SendVerificationEmail(userIdentity.Email, userIdentity.UserName, confirmationUrl);
        }

        [Route(ApiRoutes.UpdateUserProfilePasswordAsync)]
        public async Task<ApiResponse> UpdateUserProfilePasswordAsync([FromBody] UpdateUserProfilePasswordApiViewModel updateUserPassword)
        {
            var errors = new List<string>();

            var user = await _userManager.GetUserAsync(HttpContext.User);

            if (user.IsNull())
            {
                return new ApiResponse
                {
                    ErrorMessage = "User Not Found"
                };
            }

            var result = await _userManager.ChangePasswordAsync(user, updateUserPassword.CurrentPassword, updateUserPassword.NewPassword);

            if (result.Succeeded)
            {
                return new ApiResponse();
            }

            return new ApiResponse
            {
                ErrorMessage = result.Errors.AggregateErrors()
            };
        }
    }
}
