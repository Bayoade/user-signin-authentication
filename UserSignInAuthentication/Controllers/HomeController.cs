﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using UserSignInAuthentication.Data.Sql.Model;
using UserSignInAuthentication.Models;
using UserSignInAuthentication.Web.Models;

namespace UserSignInAuthentication.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public HomeController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        [Route("Create")]
        public async Task<IActionResult> CreateUserAsync(RegisterCredentialsViewModel registerCredentials)
        {
            var applicationUser = new ApplicationUser
            {
                UserName = registerCredentials.UserName,
                FirstName = registerCredentials.FirstName,
                LastName = registerCredentials.LastName,
                Email = registerCredentials.Email
            };

            var result = await _userManager.CreateAsync(applicationUser, registerCredentials.Password);

            if (result.Succeeded)
            {
                //var userIdentity = await _userManager.FindByNameAsync(registerCredentials.UserName);

                //var emailVerification = _userManager.GenerateEmailConfirmationTokenAsync(applicationUser);

                return Content("A new user has being created successfully", "text/html");
            }

            return Content("User creation failed", "text/html");
        }

        /// <summary>
        /// Private area
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [Route("private")]
        public IActionResult Private()
        {
            return Content($"This is a private area. Welcome {HttpContext.User.Identity.Name}");
        }

        /// <summary>
        /// An auto-login page for testing
        /// </summary>
        /// <param name="returnUrl">The url to return to on successful login</param>
        /// <returns></returns>
        [Route("login")]
        public async Task<IActionResult> LoginAsync(string returnUrl)
        {
            // Sign out any previous sessions
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);

            // Sign user in with the valid credentials
            var result = await _signInManager.PasswordSignInAsync("yum-yum", "yum-yum", true, false);

            if (result.Succeeded)
            {
                if (!string.IsNullOrEmpty(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return RedirectToAction(nameof(Index));
            }

            return Content("Failed to login", "text/html");
        }

        [Route("logout")]
        public async Task<IActionResult> SignOutAsync()
        {
            // signout from identity scheme form of logins
            await HttpContext.SignOutAsync(IdentityConstants.ApplicationScheme);

            return Content($"User is logged out of system");
        }



        //public IActionResult Privacy()
        //{
        //    return View();
        //}

        //[ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        //public IActionResult Error()
        //{
        //    return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        //}
    }
}
