﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserSignInAuthentication.Core.Helpers
{
    public static class ObjectExtensionMethods
    {
        public static bool IsNull<T>(this T source)
        {
            return source == null;
        }
    }
}
