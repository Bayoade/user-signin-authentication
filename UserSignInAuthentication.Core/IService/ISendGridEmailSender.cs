﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using UserSignInAuthentication.Core.Model;

namespace UserSignInAuthentication.Core.IService
{
    public interface ISendGridEmailSender
    {
        Task<SendEmailResponse> SendEmailAsync(SendEmailDetails senderDetails);
    }
}
