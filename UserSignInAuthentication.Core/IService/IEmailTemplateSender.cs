﻿using System.Threading.Tasks;
using UserSignInAuthentication.Core.Model;

namespace UserSignInAuthentication.Core.IService
{
    public interface IEmailTemplateSender
    {
        Task<SendEmailResponse> SendGeneralEmailAsync(SendEmailDetails sendEmail, string title, string content1, string content2, string buttonText, string buttonUrl);
    }
}
