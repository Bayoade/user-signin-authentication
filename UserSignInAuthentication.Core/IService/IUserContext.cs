﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserSignInAuthentication.Core.IService
{
    public interface IUserContext
    {
        bool IsAuthenticated();

        string UserName();
    }
}
