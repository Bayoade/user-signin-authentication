﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserSignInAuthentication.Core.Model
{
    public class SendEmailDetails
    {
        public string ToEmail { get; set; }

        public string ToName { get; set; }

        public string FromEmail { get; set; }

        public string FromName { get; set; }

        public string  Subject { get; set; }

        public bool IsHtml { get; set; }

        public string Contents { get; set; }

        //public string ToEmail { get; set; }
        //public string ToEmail { get; set; }
        //public string ToEmail { get; set; }
    }
}
