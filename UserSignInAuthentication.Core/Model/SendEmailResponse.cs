﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserSignInAuthentication.Core.Model
{
    public class SendEmailResponse
    {
        public bool Successful => !(Errors?.Count > 0);

        public IList<string> Errors { get; set; }

    }
}
