﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserSignInAuthentication.Core.Model
{
    public class SendGridResponse
    {
        public IList<SendGridResponseError> Errors { get; set; }
    }
}
