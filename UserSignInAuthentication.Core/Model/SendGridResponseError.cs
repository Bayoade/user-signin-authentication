﻿namespace UserSignInAuthentication.Core.Model
{
    public class SendGridResponseError
    {
        public string Message { get; set; }

        public string Help { get; set; }

        public string Field { get; set; }
    }
}
