﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using UserSignInAuthentication.Data.Sql.Model;

namespace UserSignInAuthentication.Data.Sql.Context
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) 
            : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            var modebuilderUserSettings = modelBuilder
                .Entity<UserSetting>()
                .ToTable("UserSettings");

            modebuilderUserSettings.HasIndex(x => x.Name);
        }

        public DbSet<UserSetting> UserSettings;
    }
}
