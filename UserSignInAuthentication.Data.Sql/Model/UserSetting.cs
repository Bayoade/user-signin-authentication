﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UserSignInAuthentication.Data.Sql.Model
{
    public class UserSetting
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }
    }
}
